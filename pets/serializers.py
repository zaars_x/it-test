from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator
from rest_framework import serializers

from pets.models import Cat, Breed


USER_MODEL = get_user_model()


class CatSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(queryset=USER_MODEL.objects.all())

    class Meta:
        model = Cat
        fields = ['name', 'breed', 'date_of_birth', 'hairiness', 'owner']

    def create(self, validated_data):

        res = super().create(validated_data)
        return res


class BreedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Breed
        fields = ['title',]



