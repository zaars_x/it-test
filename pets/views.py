from django.shortcuts import render
from rest_framework import viewsets, permissions

from pets.models import Cat, Breed
from pets.serializers import CatSerializer, BreedSerializer


class CatViewSet(viewsets.ModelViewSet):

    queryset = Cat.objects.all()
    serializer_class = CatSerializer
    permission_classes = [permissions.AllowAny]
    # permission_classes = [permissions.IsAuthenticated]


class BreedViewSet(viewsets.ModelViewSet):

    queryset = Breed.objects.all()
    serializer_class = BreedSerializer
    permission_classes = [permissions.AllowAny]


