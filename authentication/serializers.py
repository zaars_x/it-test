from rest_framework import serializers

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


USER_MODEL = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True, style={'input_type': 'password'},)
    password2 = serializers.CharField(write_only=True, style={'input_type': 'password'},)

    class Meta:
        model = USER_MODEL
        fields = ['username', 'first_name', 'last_name', 'email', 'password', 'password2']

    def validate(self, attrs):
        data = super().validate(attrs)
        if data['password'] != data['password2']:
            raise serializers.ValidationError(_('Password mismatch'))
        del data['password2']
        return data

    def create(self, validated_data):
        user = USER_MODEL(
            email=validated_data['email'],
            username=validated_data['username'],
            last_name=validated_data['last_name'],
            first_name=validated_data['first_name'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, user, validated_data):
        user.last_name=validated_data['last_name'],
        user.first_name=validated_data['first_name'],
        user.set_password(validated_data['password'])
        user.save()
        return user
