from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _


from django_enumfield import enum


USER_MODEL = get_user_model()


class Pet(models.Model):

    class Meta:
        verbose_name = _('pet')
        abstract = True

        ordering = ['name']

    name = models.CharField(
        verbose_name=_('name'),
        max_length=200,
    )

    def __str__(self):
        return f'{self._meta.verbose_name}: {self.name}'


class Breed(models.Model):

    class Meta:
        verbose_name = _('breed')

        ordering = ['title']

    title = models.CharField(
        verbose_name=_('title'),
        max_length=200,
    )

    def __str__(self):
        return self.title


class Hairiness(enum.Enum):
    NONE = 0
    LOW = 1
    MEDIUM = 2
    HIGH = 3

    __labels__ = {
        NONE: _('none'),
        LOW: _('low'),
        MEDIUM: _('medium'),
        HIGH: _('high'),
    }




class Cat(Pet):

    class Meta(Pet.Meta):
        verbose_name = _('cat')

    breed = models.ForeignKey(
        to=Breed,
        on_delete=models.CASCADE,
    )

    date_of_birth = models.DateField(
        verbose_name=_('date of birth'),
        null=True,
        blank=True,
    )

    hairiness = enum.EnumField(
        Hairiness,
        verbose_name=_('hairiness'),
        default=Hairiness.LOW,
    )

    owner = models.ForeignKey(
        to=USER_MODEL,
        on_delete=models.CASCADE,

        blank=True,
        null=True,
    )
